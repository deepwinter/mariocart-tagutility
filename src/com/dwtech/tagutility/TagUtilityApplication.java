package com.dwtech.tagutility;

import com.dwtech.tagutility.model.SettingsContract;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class TagUtilityApplication extends Application {

	private static TagUtilityApplication application;
	
	public static TagUtilityApplication getApplication(){
		
		return application;
	}
	
	public String getBaseUrl(){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		return settings.getString(SettingsContract.BASE_URL, SettingsContract.DEFAULT_BASE_URL);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		
		application = this;
	}
	
	
}
