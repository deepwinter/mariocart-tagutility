package com.dwtech.tagutility.activities;

import com.dwtech.tagutility.R;
import com.dwtech.tagutility.TagUtilityApplication;
import com.dwtech.tagutility.R.layout;
import com.dwtech.tagutility.R.menu;
import com.dwtech.tagutility.activities.base.TagUtilityBaseActivity;
import com.dwtech.tagutility.api.AssetPut;
import com.dwtech.tagutility.api.EmptyResponse;
import com.dwtech.tagutility.api.KartAssociationPost;
import com.dwtech.tagutility.api.KartAssociationResponse;
import com.octo.android.robospice.JacksonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.exception.NoNetworkException;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class AssociateTagsActivity extends TagUtilityBaseActivity {

	public  SpiceManager spiceManager = new SpiceManager( JacksonSpringAndroidSpiceService.class );
	
	private TextView tagOneText;
	private TextView tagTwoText;
	private Button uploadButton;
	private Button clearButton;
	private TextView associateTagsLastAction;
	private PendingIntent pendingIntent;
	private String tagOneId;
	private String tagTwoId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_associate_tags);
		
		tagOneText = (TextView) findViewById(R.id.tagOneText);
		tagTwoText = (TextView) findViewById(R.id.tagTwoText);
		uploadButton = (Button) findViewById(R.id.associatorUploadButton);
		clearButton = (Button) findViewById(R.id.clearButton);
		associateTagsLastAction = (TextView) findViewById(R.id.associateTagsLastAction);
		
		clearButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				clearTags();				
			}
			
		});
		
		uploadButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				if(tagOneId == null || tagTwoId == null){
					// Don't have both tags, don't upload
					return;
				}
				
				// Attempt upload
				
				class KartAssociationPostListener implements RequestListener< KartAssociationResponse > {

					@Override
					public void onRequestFailure(SpiceException e) {
						//showProgress(false);
						if(e instanceof NoNetworkException){
							Toast.makeText(getApplicationContext(), "Network connection is unavailable", Toast.LENGTH_LONG).show();
							spiceManager.cancelAllRequests();
						} else {
							Toast.makeText(getApplicationContext(), "Error during request: " + e.getMessage(), Toast.LENGTH_LONG).show();
							e.printStackTrace();
						}								
					}

					@Override
					public void onRequestSuccess(KartAssociationResponse response) {
						clearTags();
						String text = "Guest " + response.guestName + " assigned to kart " + response.kartLabel;
						Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
						associateTagsLastAction.setText(text);
					}
					
				}
				
				spiceManager.execute( new KartAssociationPost(TagUtilityApplication.getApplication().getBaseUrl(), tagOneId, tagTwoId), 
						null, DurationInMillis.NEVER, new KartAssociationPostListener() );
				
			}
			
			
			
		});
		
	}



	@Override
	protected void onStart() {
		super.onStart();
		spiceManager.start( this );
	}
	
	@Override
	protected void onStop() {
		if(spiceManager.isStarted()){
			spiceManager.shouldStop();
		}
		super.onStop();
	}
	
	protected void onResume() {
		super.onResume();

		pendingIntent = PendingIntent.getActivity(
				this, 0, new Intent(this, AssociateTagsActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP ), 0);
		IntentFilter tagDiscoveredFilter = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
		IntentFilter intentFiltersArray[] = new IntentFilter[] {tagDiscoveredFilter };

		NfcAdapter.getDefaultAdapter(this).enableForegroundDispatch(this, pendingIntent, intentFiltersArray, null);


	}

	@Override
	protected void onNewIntent (Intent intent) {
		super.onNewIntent(intent);

		setIntent(intent);

		handleNFCTag();

	}

	private void handleNFCTag(){

		Tag myTag = (Tag) getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);
		String tagId = ByteArrayToHexString(myTag.getId());

		if(tagOneId == null){
			tagOneId = tagId;
			tagOneText.setText(tagId);
		} else if( tagTwoId == null) {
			tagTwoId = tagId;
			tagTwoText.setText(tagId);
		}

	}

	String ByteArrayToHexString(byte [] inarray) 
	{
		int i, j, in;
		String [] hex = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
		String out= "";

		for(j = 0 ; j < inarray.length ; ++j) 
		{
			in = (int) inarray[j] & 0xff;
			i = (in >> 4) & 0x0f;
			out += hex[i];
			i = in & 0x0f;
			out += hex[i];
		}
		return out;
	}

	private void clearTags() {
		tagOneId = null;;
		tagOneText.setText("");		
		tagTwoId = null;;
		tagTwoText.setText("");
	}


}
