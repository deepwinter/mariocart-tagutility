package com.dwtech.tagutility.activities;

import com.dwtech.tagutility.R;
import com.dwtech.tagutility.R.id;
import com.dwtech.tagutility.R.layout;
import com.dwtech.tagutility.R.menu;
import com.dwtech.tagutility.activities.base.TagUtilityBaseActivity;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends TagUtilityBaseActivity {

	Button assignTagsButton;
	Button associateTagsButton;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        associateTagsButton = (Button) findViewById(R.id.associateTagsButton);
        
        associateTagsButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(MainActivity.this, AssociateTagsActivity.class);
				startActivity(i);
				
			}
        	
        });
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		Intent intent;
		switch (item.getItemId()) {
		
		case R.id.action_assign_tags:
			intent = new Intent(this, AssignTagsActivity.class);
			startActivity(intent);
			return true;
			
		default:
			return super.onOptionsItemSelected(item);
		}

	}
    
}
