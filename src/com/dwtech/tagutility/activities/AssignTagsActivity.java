package com.dwtech.tagutility.activities;

import com.dwtech.tagutility.R;
import com.dwtech.tagutility.R.layout;
import com.dwtech.tagutility.R.menu;
import com.dwtech.tagutility.TagUtilityApplication;
import com.dwtech.tagutility.activities.base.TagUtilityBaseActivity;
import com.dwtech.tagutility.api.AssetPut;
import com.dwtech.tagutility.model.Asset;
import com.dwtech.tagutility.model.AssetsContract;
import com.dwtech.tagutility.model.AssetsProvider;
import com.dwtech.tagutility.model.Thing;
import com.octo.android.robospice.JacksonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.exception.NoNetworkException;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import android.net.Uri;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.CancellationSignal.OnCancelListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.LoaderManager;
import android.app.PendingIntent;
import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.database.Cursor;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemLongClickListener;

public class AssignTagsActivity extends TagUtilityBaseActivity implements LoaderManager.LoaderCallbacks<Cursor>  {

	private static final int OBJECT_TYPES_LOADER_ID = 0;
	private static final int OBJECTS_LOADER_ID = 1;
	private static final int TAGGED_ASSETS_LOADER_ID = 2;

	public  SpiceManager spiceManager = new SpiceManager( JacksonSpringAndroidSpiceService.class );

	private Spinner objectTypeSpinner;
	private Spinner objectSpinner;
	private ListView taggedAssetsList;
	private SimpleCursorAdapter objectSpinnerAdapter;
	private SimpleCursorAdapter taggedAssetsListAdapter;

	private PendingIntent pendingIntent;
	private TextView tagView;
	private String currentTagId;
	private Button saveButton;
	private Button uploadButton;
	
	private Cursor assetsCursor;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_assign_tags);

		tagView = (TextView) findViewById(R.id.tagIdText);
		objectTypeSpinner = (Spinner) findViewById(R.id.objectTypeSpinner);
		objectSpinner = (Spinner) findViewById(R.id.objectSpinner);
		taggedAssetsList = (ListView) findViewById(R.id.taggedAssetsList);
		saveButton = (Button) findViewById(R.id.saveButton);
		uploadButton = (Button) findViewById(R.id.uploadButton);

		String from[] = { AssetsContract.NAME_FIELD};
		int to[] = { android.R.id.text1 };
		objectSpinnerAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, null, from, to, 0);
		objectSpinner.setAdapter(objectSpinnerAdapter);

		String from2[] = { AssetsContract.NAME_FIELD, AssetsContract.TAG_ID_FIELD };
		int to2[] = { android.R.id.text1, android.R.id.text2 };
		taggedAssetsListAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, null, from2, to2, 0);
		taggedAssetsList.setAdapter(taggedAssetsListAdapter);

		saveButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Asset asset = new Asset();
				Cursor c = objectSpinnerAdapter.getCursor();
				Integer position = objectSpinner.getSelectedItemPosition();
				if(position == -1){
					return;
				}
				c.moveToPosition(objectSpinner.getSelectedItemPosition());
				asset.assetId = c.getString(c.getColumnIndex(AssetsContract.ASSET_ID_FIELD));
				asset.type =  c.getString(c.getColumnIndex(AssetsContract.TYPE_FIELD));
				asset.name =  c.getString(c.getColumnIndex(AssetsContract.NAME_FIELD));
				asset.tag = currentTagId;
				getContentResolver().insert(AssetsProvider.ASSETS_CONTENT_URI, asset.getContentValues());
				currentTagId = null;
				tagView.setText("");
				taggedAssetsListAdapter.notifyDataSetChanged();
			}

		});

		uploadButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {

				if(assetsCursor == null){
					return;
				}

				int count = assetsCursor.getCount();
				Asset[] assets = new Asset[count];
				for(int i=0; i<count; i++){
					assetsCursor.moveToPosition(i);
					Asset asset = new Asset();
					asset.assetId = assetsCursor.getString(assetsCursor.getColumnIndex(AssetsContract.ASSET_ID_FIELD));					
					asset.name = assetsCursor.getString(assetsCursor.getColumnIndex(AssetsContract.NAME_FIELD));
					asset.tag = assetsCursor.getString(assetsCursor.getColumnIndex(AssetsContract.TAG_ID_FIELD));
					asset.type = assetsCursor.getString(assetsCursor.getColumnIndex(AssetsContract.TYPE_FIELD));
					asset.latitude = assetsCursor.getDouble(assetsCursor.getColumnIndex(AssetsContract.LATITUDE_FIELD));
					asset.longitude = assetsCursor.getDouble(assetsCursor.getColumnIndex(AssetsContract.LONGITUDE_FIELD));
					asset.location = assetsCursor.getString(assetsCursor.getColumnIndex(AssetsContract.LOCATION_FIELD));
					asset.ProviderId = assetsCursor.getInt(assetsCursor.getColumnIndex(AssetsContract.ID_FIELD));
					assets[i] = asset;
				}

				for(int i=0; i<count; i++){

					final Asset asset = assets[i];

					class AssetPutListener implements RequestListener< Boolean > {

						@Override
						public void onRequestFailure(SpiceException e) {
							//showProgress(false);
							if(e instanceof NoNetworkException){
								Toast.makeText(getApplicationContext(), "Network connection is unavailable", Toast.LENGTH_LONG).show();
								spiceManager.cancelAllRequests();
							} else {
								Toast.makeText(getApplicationContext(), "Error during request: " + e.getMessage(), Toast.LENGTH_LONG).show();
								e.printStackTrace();
							}				
						}

						@Override
						public void onRequestSuccess(Boolean success) {


							getContentResolver().delete(
									ContentUris.withAppendedId(AssetsProvider.ASSETS_CONTENT_URI, asset.ProviderId),
									null,
									null
									);
							taggedAssetsListAdapter.notifyDataSetChanged();

						}

					}

					spiceManager.execute( new AssetPut(TagUtilityApplication.getApplication().getBaseUrl(), asset), 
							null, DurationInMillis.NEVER, new AssetPutListener() );

				}
			}

		});

		taggedAssetsList.setOnItemLongClickListener(new OnItemLongClickListener(){

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					final int position, long arg3) {

				class ConfirmDeleteDialoge extends DialogFragment implements OnCancelListener {

					@Override
					public Dialog onCreateDialog(Bundle savedInstanceState) {

						AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
						builder.setMessage("Are you sure you want to delete this item?").setTitle("Delete item?");

						// Add the buttons
						builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog, int id) {

								ConfirmDeleteDialoge.this.dismiss();

								long assetRecordId = taggedAssetsListAdapter.getItemId(position);
								Uri uri = ContentUris.withAppendedId(AssetsProvider.ASSETS_CONTENT_URI, assetRecordId);
								getContentResolver().delete(uri, null, null);


							}
						});

						builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								ConfirmDeleteDialoge.this.dismiss();
							}
						});

						AlertDialog dialog = builder.create();
						return dialog;
					}

					@Override
					public void onCancel() {
						ConfirmDeleteDialoge.this.dismiss();
					}
				}

				ConfirmDeleteDialoge dialog = new ConfirmDeleteDialoge();
				dialog.show(AssignTagsActivity.this.getFragmentManager(), "dialog");	

				return false;
			}

		});
		

		LoaderManager lm = getLoaderManager();
		lm.initLoader(OBJECT_TYPES_LOADER_ID, null, this);
		lm.initLoader(OBJECTS_LOADER_ID, null, this);
		lm.initLoader(TAGGED_ASSETS_LOADER_ID, null, this);

	}

	@Override
	protected void onStart() {
		super.onStart();
		spiceManager.start( this );
	}
	
	@Override
	protected void onStop() {
		if(spiceManager.isStarted()){
			spiceManager.shouldStop();
		}
		super.onStop();
	}
	



	protected void onResume() {
		super.onResume();

		pendingIntent = PendingIntent.getActivity(
				this, 0, new Intent(this, AssignTagsActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP ), 0);
		IntentFilter tagDiscoveredFilter = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
		IntentFilter intentFiltersArray[] = new IntentFilter[] {tagDiscoveredFilter };

		NfcAdapter.getDefaultAdapter(this).enableForegroundDispatch(this, pendingIntent, intentFiltersArray, null);


	}

	@Override
	protected void onNewIntent (Intent intent) {
		super.onNewIntent(intent);

		setIntent(intent);

		handleNFCTag();

	}

	private void handleNFCTag(){

		Tag myTag = (Tag) getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);
		currentTagId = ByteArrayToHexString(myTag.getId());

		tagView.setText(currentTagId);

		/*
		 * GPSTracker Disabled in this Build
		if(GPSTracker.canGetLocation()){
			tagLatitude = GPSTracker.getLatitude();
			tagLongitude = GPSTracker.getLongitude();
		} else {
			Toast.makeText(this, "Could not get Lat/Lon for this tag, but you might not care about this", Toast.LENGTH_LONG).show();
		}
		 */

	}

	String ByteArrayToHexString(byte [] inarray) 
	{
		int i, j, in;
		String [] hex = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
		String out= "";

		for(j = 0 ; j < inarray.length ; ++j) 
		{
			in = (int) inarray[j] & 0xff;
			i = (in >> 4) & 0x0f;
			out += hex[i];
			i = in & 0x0f;
			out += hex[i];
		}
		return out;
	}




	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {

		if(id == OBJECTS_LOADER_ID) {

			// Create a new CursorLoader with the following query parameters.
			return new CursorLoader(this, 
					AssetsProvider.OBJECTS_CONTENT_URI,
					Thing.getDefaultProjection(), null, null, AssetsContract.ID_FIELD);
		} else if (id == TAGGED_ASSETS_LOADER_ID) {

			// Create a new CursorLoader with the following query parameters.
			return new CursorLoader(this, 
					AssetsProvider.ASSETS_CONTENT_URI,
					Asset.getDefaultProjection(), null, null, null);	

		}  
		return null;
	}

	/*
	private void switchAdapter(){
		if(itemsDisplayType == VEHICLES_TYPE){
			mItemSpinner.setAdapter(vehiclesAdapter);
		} else if (itemsDisplayType == SMART_POSTERS_TYPE) {
			mItemSpinner.setAdapter(smartPostersAdapter);
		} else if (itemsDisplayType == POINTS_OF_INTEREST_TYPE) {
			mItemSpinner.setAdapter(pointsOfInterestAdapter);
		}
	}
	 */

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		if (loader.getId() == OBJECTS_LOADER_ID) {
			// The asynchronous load is complete and the data
			// is now available for use. Only now can we associate
			// the queried Cursor with the SimpleCursorAdapter.
			objectSpinnerAdapter.swapCursor(cursor);
			objectSpinnerAdapter.notifyDataSetChanged();

		} else if (loader.getId() == TAGGED_ASSETS_LOADER_ID) {

			// smart posters
			assetsCursor = cursor;
			taggedAssetsListAdapter.swapCursor(cursor);
			taggedAssetsListAdapter.notifyDataSetChanged();

		}

	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		// For whatever reason, the Loader's data is now unavailable.
		// Remove any references to the old data by replacing it with
		// a null Cursor.
		if (loader.getId() == OBJECTS_LOADER_ID) {
			objectSpinnerAdapter.swapCursor(null);
			objectSpinnerAdapter.notifyDataSetChanged();

		} else if (loader.getId() == TAGGED_ASSETS_LOADER_ID) {
			taggedAssetsListAdapter.swapCursor(null);
			taggedAssetsListAdapter.notifyDataSetChanged();

		} 
	}

}
