package com.dwtech.tagutility.activities;

import java.util.Iterator;

import com.dwtech.tagutility.R;
import com.dwtech.tagutility.TagUtilityApplication;
import com.dwtech.tagutility.activities.base.TagUtilityBaseActivity;
import com.dwtech.tagutility.api.ThingsRequest;
import com.dwtech.tagutility.api.ThingsResponse;
import com.dwtech.tagutility.model.AssetsContract;
import com.dwtech.tagutility.model.AssetsProvider;
import com.dwtech.tagutility.model.Thing;
import com.octo.android.robospice.JacksonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.exception.NoNetworkException;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.Toast;

public class DownloadIndexes extends TagUtilityBaseActivity {

	public SpiceManager spiceManager = new SpiceManager( JacksonSpringAndroidSpiceService.class );


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_download_indexes);
	}

	@Override
	protected void onStart() {
		super.onStart();
		spiceManager.start( this );
		reloadIndexes();
	}

	@Override
	protected void onStop() {
		if(spiceManager.isStarted()){
			spiceManager.shouldStop();
		}
		super.onStop();
	}


	private void reloadIndexes(){
		
		getThings("kart");

	}

	private void getThings(final String objectType) {
		
		// Download Objects
		class ThingsRequestListener implements RequestListener< ThingsResponse > {

			@Override
			public void onRequestFailure(SpiceException e) {
				//showProgress(false);
				if(e instanceof NoNetworkException){
					Toast.makeText(getApplicationContext(), "Network connection is unavailable", Toast.LENGTH_LONG).show();
					spiceManager.cancelAllRequests();
				} else {
					Toast.makeText(getApplicationContext(), "Error during request: " + e.getMessage(), Toast.LENGTH_LONG).show();
					e.printStackTrace();
				}				
			}

			@Override
			public void onRequestSuccess(ThingsResponse things) {

				if(things == null){
					return;
				}

				// Remove all cars from the database ?
				getContentResolver().delete(
						AssetsProvider.OBJECTS_CONTENT_URI,
						AssetsContract.TYPE_FIELD + "='" + objectType + "'",
						null
						);

				Iterator<Thing> it = things.iterator();
				while (it.hasNext()) {
					Thing thing = it.next();
					getContentResolver().insert(
							AssetsProvider.OBJECTS_CONTENT_URI,   // the user dictionary content URI
							thing.getContentValues()      // the values to insert
							);
				}
				Toast.makeText(getApplicationContext(), "Done with " + objectType + "s", Toast.LENGTH_SHORT).show();

			}

		}

		spiceManager.execute( new ThingsRequest(TagUtilityApplication.getApplication().getBaseUrl(), objectType), null, DurationInMillis.NEVER, new ThingsRequestListener() );
		
	}



}
