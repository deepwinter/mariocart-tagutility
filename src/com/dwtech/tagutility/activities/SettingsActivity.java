package com.dwtech.tagutility.activities;

import com.dwtech.tagutility.R;
import com.dwtech.tagutility.R.layout;
import com.dwtech.tagutility.R.menu;
import com.dwtech.tagutility.activities.base.TagUtilityBaseActivity;
import com.dwtech.tagutility.model.SettingsContract;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class SettingsActivity extends TagUtilityBaseActivity {

	private EditText serverBaseURLText;
	private Button settingsOkButton;
	private Button downloadConfigButton;
	private SharedPreferences settings;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		
		settings =  PreferenceManager.getDefaultSharedPreferences(this);


		serverBaseURLText = (EditText) findViewById(R.id.serverBaseURLText);
		serverBaseURLText.setText(settings.getString(SettingsContract.BASE_URL, SettingsContract.DEFAULT_BASE_URL));
		
		serverBaseURLText.addTextChangedListener(new TextWatcher() {

			public void afterTextChanged(Editable s) {
				saveUrl();
			}    
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {                

			}    
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

			}    
		});
		
		settingsOkButton = (Button) findViewById(R.id.settingsOkButton);
		settingsOkButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				saveUrl();
				finish();
			}
			
		});
		
		downloadConfigButton = (Button) findViewById(R.id.settingsDownloadConfigButton);
		downloadConfigButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(SettingsActivity.this, DownloadIndexes.class);
				startActivity(intent);				
			}
			
		})
;	}
	
	private void saveUrl(){
		Editor edit = settings.edit();
		edit.putString(SettingsContract.BASE_URL, serverBaseURLText.getText().toString());
		edit.apply();
	}



}
