package com.dwtech.tagutility.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import android.content.ContentValues;

@JsonIgnoreProperties(ignoreUnknown = true)

public class Thing {

	public Integer id;
	public String name;
	public String type;
	
	public ContentValues getContentValues() {
		ContentValues cv = new ContentValues();
		cv.put(AssetsContract.ASSET_ID_FIELD, id);
		cv.put(AssetsContract.NAME_FIELD, name);
		cv.put(AssetsContract.TYPE_FIELD, type);
		return cv;
	}

	public static String[] getDefaultProjection() {
		return new String[]{ AssetsContract.ID_FIELD, 
				AssetsContract.ASSET_ID_FIELD, 
				AssetsContract.NAME_FIELD, 
				AssetsContract.TYPE_FIELD
		};
	}
	
}
