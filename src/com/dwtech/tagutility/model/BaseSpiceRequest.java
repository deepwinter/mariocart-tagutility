package com.dwtech.tagutility.model;

import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public abstract class BaseSpiceRequest<T> extends SpringAndroidSpiceRequest<T> {
	
	public BaseSpiceRequest(Class<T> clazz) {
		super(clazz);
	}

	public static final int WEBSERVICES_TIMEOUT = 30 * 1000; // seconds ??

	// TODO all rest templates should extend this
	public void manageTimeOuts(RestTemplate restTemplate) {
		// set timeout for requests
		ClientHttpRequestFactory factory = restTemplate.getRequestFactory();
		if (factory instanceof HttpComponentsClientHttpRequestFactory) {
			HttpComponentsClientHttpRequestFactory advancedFactory = (HttpComponentsClientHttpRequestFactory) factory;
			advancedFactory.setConnectTimeout(WEBSERVICES_TIMEOUT);
			advancedFactory.setReadTimeout(WEBSERVICES_TIMEOUT);
		} else if (factory instanceof SimpleClientHttpRequestFactory) {
			SimpleClientHttpRequestFactory advancedFactory = (SimpleClientHttpRequestFactory) factory;
			advancedFactory.setConnectTimeout(WEBSERVICES_TIMEOUT);
			advancedFactory.setReadTimeout(WEBSERVICES_TIMEOUT);
		}
	}
	
	public RestTemplate getRestTemplate(){
		RestTemplate restTemplate = super.getRestTemplate();
		manageTimeOuts(restTemplate);
		return restTemplate;
		
	}
	
	
}
