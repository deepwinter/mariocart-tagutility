package com.dwtech.tagutility.model;

public class AssetsContract {

	public static final String ID_FIELD = "_id";
	public static final String NAME_FIELD = "Name";
	public static final String TYPE_FIELD = "Type";
	public static final String LOCATION_FIELD = "Location";
	public static final String TAG_ID_FIELD = "TagId";
	public static final String ASSET_ID_FIELD = "AssetId";
	public static final String LONGITUDE_FIELD = "Longitude";
	public static final String LATITUDE_FIELD = "Latitude";

	// Object Types
	public static final String KART_TYPE = "Kart";


}
