package com.dwtech.tagutility.model;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import android.content.ContentValues;



@JsonIgnoreProperties(ignoreUnknown = true)
public class Asset {

	public String name;
	public String type;
	public String location;
	public String tag;
	public String assetId;
	public double latitude;
	public double longitude;
	
	// Internal
	@JsonIgnore
	public int ProviderId;
	
	@JsonIgnore
	public ContentValues getContentValues(){
		ContentValues values = new ContentValues();
		values.put(AssetsContract.NAME_FIELD, name);
		values.put(AssetsContract.TAG_ID_FIELD, tag);
		values.put(AssetsContract.ASSET_ID_FIELD, assetId);
		values.put(AssetsContract.TYPE_FIELD, type);
		values.put(AssetsContract.LATITUDE_FIELD, latitude);
		values.put(AssetsContract.LONGITUDE_FIELD, longitude);
		return values;
	}

	public static String[] getDefaultProjection() {
		return new String[]{ AssetsContract.ID_FIELD, 
				AssetsContract.NAME_FIELD, 
				AssetsContract.TYPE_FIELD,
				AssetsContract.TAG_ID_FIELD, 
				AssetsContract.ASSET_ID_FIELD, 
				AssetsContract.LOCATION_FIELD,
				AssetsContract.LATITUDE_FIELD,
				AssetsContract.LONGITUDE_FIELD
		};
	}
	
}
