package com.dwtech.tagutility.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class TagUtilityDatabase extends SQLiteOpenHelper {

	private static final String DEBUG_TAG = "TagUtilityDatabase";
	private static final int DB_VERSION = 1;
	private static final String DB_NAME = "TagUtilityDatabase";

	// Objects are things that are in the remote database and can be assigned
	public static final String TABLE_OBJECTS = "objects";
	public static final String CREATE_TABLE_OBJECTS = " create table " + TABLE_OBJECTS
			+ " (" + AssetsContract.ID_FIELD + " integer primary key autoincrement, "
			+ AssetsContract.NAME_FIELD  + " text, "
			+ AssetsContract.TYPE_FIELD + " text not null, "
			+ AssetsContract.ASSET_ID_FIELD + " text not null "
			+");";


	// Assets are tag -> object assignments
	public static final String TABLE_ASSETS = "assets";
	public static final String CREATE_TABLE_ASSETS = " create table " + TABLE_ASSETS
			+ " (" + AssetsContract.ID_FIELD  + " integer primary key autoincrement, " 
			+ AssetsContract.NAME_FIELD  + " text, "
			+ AssetsContract.TAG_ID_FIELD + " text not null, "
			+ AssetsContract.TYPE_FIELD + " text not null, "
			+ AssetsContract.ASSET_ID_FIELD + " text not null, "
			+ AssetsContract.LOCATION_FIELD + " text, "
			+ AssetsContract.LATITUDE_FIELD + " double, "
			+ AssetsContract.LONGITUDE_FIELD + " double "
			+");";



	public TagUtilityDatabase(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_ASSETS);
		db.execSQL(CREATE_TABLE_OBJECTS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(DEBUG_TAG, "Upgrading database. Existing contents will be lost. ["
				+ oldVersion + "]->[" + newVersion + "]");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ASSETS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_OBJECTS);
		onCreate(db);
	}
}


