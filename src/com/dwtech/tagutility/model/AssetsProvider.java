package com.dwtech.tagutility.model;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;


public class AssetsProvider extends ContentProvider {

	private static final String AUTHORITY = "com.dwtech.tagutility.provider.AssetsProvider";
	public static final int ASSETS = 100;
	public static final int ASSET = 110;
	public static final int OBJECTS = 200;
	public static final int OBJECT = 210;
	private static final String ASSETS_BASE_PATH = "assets";
	private static final String ASSETS_SEGMENT = "assets";
	private static final String OBJECTS_SEGMENT = "objects";
	private static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
	        + "/" + ASSETS_BASE_PATH);
	public static final Uri ASSETS_CONTENT_URI = Uri.parse("content://" + AUTHORITY
	        + "/" + ASSETS_BASE_PATH + "/" + ASSETS_SEGMENT);
	public static final Uri OBJECTS_CONTENT_URI = Uri.parse("content://" + AUTHORITY
	        + "/" + ASSETS_BASE_PATH + "/" + OBJECTS_SEGMENT);
	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
	        + "/asset";
	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
	        + "/asset";
	
	private TagUtilityDatabase mDB;
	
	private static final UriMatcher sURIMatcher = new UriMatcher(
	        UriMatcher.NO_MATCH);
	static {
	    sURIMatcher.addURI(AUTHORITY, ASSETS_BASE_PATH + "/" + ASSETS_SEGMENT, ASSETS);
	    sURIMatcher.addURI(AUTHORITY, ASSETS_BASE_PATH + "/" + ASSETS_SEGMENT + "/*", ASSET);
	    sURIMatcher.addURI(AUTHORITY, ASSETS_BASE_PATH + "/" + OBJECTS_SEGMENT, OBJECTS);
	    sURIMatcher.addURI(AUTHORITY, ASSETS_BASE_PATH + "/" + OBJECTS_SEGMENT + "/*", OBJECT);
	}
	
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
	    int uriType = sURIMatcher.match(uri);
	    SQLiteDatabase sqlDB = mDB.getWritableDatabase();
	    int rowsAffected = 0;
	    String id;
	    switch (uriType) {
	    case ASSETS:
	        rowsAffected = sqlDB.delete(TagUtilityDatabase.TABLE_ASSETS,
	                selection, selectionArgs);
	        break;
	        
	    case ASSET:
			id = uri.getLastPathSegment();
			rowsAffected = sqlDB.delete(TagUtilityDatabase.TABLE_ASSETS,
	                "_id = "+id, null);
	        break;
	    case OBJECTS:
	        rowsAffected = sqlDB.delete(TagUtilityDatabase.TABLE_OBJECTS,
	                selection, selectionArgs);
	        break;
	        
	    case OBJECT:
			id = uri.getLastPathSegment();
			rowsAffected = sqlDB.delete(TagUtilityDatabase.TABLE_OBJECTS,
	                "_id = "+id, null);
	        break;

	    default:
	        throw new IllegalArgumentException("Unknown or Invalid URI " + uri);
	    }
	    getContext().getContentResolver().notifyChange(uri, null);
	    return rowsAffected;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Uri result = null;

	    int uriType = sURIMatcher.match(uri);
	    SQLiteDatabase db = mDB.getWritableDatabase();
	    int rowsAffected = 0;
	    long rowID;
	    switch (uriType) {
	    case ASSETS:
	    	rowID = db.insert(TagUtilityDatabase.TABLE_ASSETS, null, values);

			if (rowID > 0) {
				// Return a URI to the newly created row on success
				result = ContentUris.withAppendedId(ASSETS_CONTENT_URI, rowID);

				// Notify the Context's ContentResolver of the change
				getContext().getContentResolver().notifyChange(result, null);
			}
			return result;		
	 
	    case OBJECTS:
	    	rowID = db.insert(TagUtilityDatabase.TABLE_OBJECTS, null, values);

			if (rowID > 0) {
				// Return a URI to the newly created row on success
				result = ContentUris.withAppendedId(OBJECTS_CONTENT_URI, rowID);

				// Notify the Context's ContentResolver of the change
				getContext().getContentResolver().notifyChange(result, null);
			}
			return result;	
			
	    default:
	        throw new IllegalArgumentException("Unknown or Invalid URI " + uri);
	    }	
	    	

		
	}

	@Override
	public boolean onCreate() {
		mDB = new TagUtilityDatabase(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
	    int uriType = sURIMatcher.match(uri);
	    Cursor cursor = null;
	    switch (uriType) {
	    case ASSET:
		    queryBuilder.setTables(TagUtilityDatabase.TABLE_ASSETS);
	        queryBuilder.appendWhere(AssetsContract.TAG_ID_FIELD + "="
	                + uri.getLastPathSegment());
	        cursor = queryBuilder.query(mDB.getReadableDatabase(),
		            projection, selection, selectionArgs, null, null, sortOrder);
		    cursor.setNotificationUri(getContext().getContentResolver(), uri);	      
		    break;
	    case ASSETS:
		    queryBuilder.setTables(TagUtilityDatabase.TABLE_ASSETS);
	        cursor = queryBuilder.query(mDB.getReadableDatabase(),
		            projection, selection, selectionArgs, null, null, sortOrder);
		    cursor.setNotificationUri(getContext().getContentResolver(), uri);	      
		    break;
	    case OBJECT:
		    queryBuilder.setTables(TagUtilityDatabase.TABLE_OBJECTS);
	        queryBuilder.appendWhere(AssetsContract.ID_FIELD + "="
	                + uri.getLastPathSegment());
	        cursor = queryBuilder.query(mDB.getReadableDatabase(),
		            projection, selection, selectionArgs, null, null, sortOrder);
		    cursor.setNotificationUri(getContext().getContentResolver(), uri);	      
		    break;
	    case OBJECTS:
		    queryBuilder.setTables(TagUtilityDatabase.TABLE_OBJECTS);
	        cursor = queryBuilder.query(mDB.getReadableDatabase(),
		            projection, selection, selectionArgs, null, null, sortOrder);
		    cursor.setNotificationUri(getContext().getContentResolver(), uri);	      
		    break;
		    
	    default:
	        throw new IllegalArgumentException("Unknown URI " + uri.toString());
	    }
	    return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}




	
}
