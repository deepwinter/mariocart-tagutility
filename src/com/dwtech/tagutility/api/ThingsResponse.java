package com.dwtech.tagutility.api;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dwtech.tagutility.model.Thing;

@JsonIgnoreProperties(ignoreUnknown = true)

public class ThingsResponse extends ArrayList<Thing> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8812169590245805658L;
	

}
