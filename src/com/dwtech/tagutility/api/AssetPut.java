package com.dwtech.tagutility.api;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.dwtech.tagutility.model.Asset;
import com.dwtech.tagutility.model.BaseSpiceRequest;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class AssetPut extends BaseSpiceRequest< Boolean > {

	String uri;
	Asset asset;
	
	public AssetPut(String baseUrl, Asset asset) {
		super(Boolean.class);
		
		// PUT 
		uri = baseUrl + "assets/upsert/" + asset.tag;
		this.asset = asset;
		
	}

	@Override
	public Boolean loadDataFromNetwork() throws Exception {
		RestTemplate restTemplate = getRestTemplate();

		/*
		 * 
		 * PUT logic
		 */
		
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<Asset> requestEntity = new HttpEntity<Asset>(asset, headers);
		
		ResponseEntity<Boolean> responseEntity = restTemplate.exchange(uri, HttpMethod.PUT, requestEntity,	Boolean.class);
		HttpStatus code = responseEntity.getStatusCode();
		if(code.equals(HttpStatus.CREATED)
				|| code.equals(HttpStatus.OK)
				|| code.equals(HttpStatus.NO_CONTENT)){
			return true;
		} else {
			return false;
		}
		
	}

}
