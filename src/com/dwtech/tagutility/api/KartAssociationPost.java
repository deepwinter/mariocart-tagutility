package com.dwtech.tagutility.api;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.dwtech.tagutility.model.BaseSpiceRequest;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class KartAssociationPost extends BaseSpiceRequest< KartAssociationResponse > {

	String uri;
	String tag1;
	String tag2;
	
	public KartAssociationPost(String baseUrl, String tag1, String tag2) {
		super(KartAssociationResponse.class);
		
		uri = baseUrl+ "game/assign/";
		this.tag1 = tag1;
		this.tag2 = tag2;
	}

	@Override
	public KartAssociationResponse loadDataFromNetwork() throws Exception {
		RestTemplate restTemplate = getRestTemplate();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		Map<String, String> params = new HashMap<String, String>();
		params.put("tag1", tag1);
		params.put("tag2", tag2);
		return restTemplate.postForObject( uri, params, KartAssociationResponse.class );	
	}

	
	
}
