package com.dwtech.tagutility.api;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.dwtech.tagutility.model.BaseSpiceRequest;
import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class ThingsRequest extends BaseSpiceRequest< ThingsResponse > {

	String uri;
	
	public ThingsRequest(String baseUrl, String objectType) {
		super(ThingsResponse.class);
		
		uri = baseUrl + objectType + "s/things/";
		
	}

	@Override
	public ThingsResponse loadDataFromNetwork() throws Exception {
		RestTemplate restTemplate = getRestTemplate();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		return restTemplate.getForObject( uri, ThingsResponse.class  );	
	}

}
